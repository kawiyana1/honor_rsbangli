﻿using Honor.Entities.SIM;
using Honor.Helper;
using Honor.Models.Honor;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Honor.Controllers.Honor
{
    [Authorize]
    public class HitungHonorController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var proses = s.HR_ListHonor.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.NoBukti.Contains(x.Value) ||
                                y.TipeHonor.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value)
                            );
                        else if (x.Key == "TipeHonor" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.TipeHonor == x.Value);

                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var _d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= _d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var _d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= _d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBukti = x.NoBukti,
                        Valisasi = x.Validasi ?? false,
                        Posted = x.Posted ?? false,
                        TglInput = x.TglInput?.ToString("dd/MM/yyyy"),
                        TipeHonor = x.TipeHonor,
                        TglStart = x.TglStart?.ToString("dd/MM/yyyy"),
                        Tanggal = x.Tanggal.ToString("dd/MM/yyyy"),
                        Keterangan = x.Keterangan,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, HitungHonorModel model)
        {
            using (var s = new SIMEntities())
            {
                //using (var dbContextTransaction = s.Database.BeginTransaction())
                //{
                try
                {
                    var userid = User.Identity.GetUserId();
                    string id = "";
                    if (_process == "CREATE")
                    {
                        var m = s.HR_HitungHonor_General(
                            model.Periode,
                            model.TipeHonor,
                            model.FilterDokter,
                            model.Keterangan
                        ).FirstOrDefault();
                        s.SaveChanges();

                        if (m != "Berhasil")
                        {
                            return JsonConvert.SerializeObject(new { IsSuccess = false, Message = m});
                        }
                    }
                    else if (_process == "DELETE")
                    {
                        id = model.NoBukti;
                        s.HR_HitungHonor_Delete(model.NoBukti);
                        s.SaveChanges();
                    }

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"HitungHonor-{_process}; nobukti:{id};".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);
                    //dbContextTransaction.Commit();

                    return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                }
                catch (SqlException ex) { return HConvert.Error(ex); }
                catch (Exception ex) { return HConvert.Error(ex); }
                //catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                //catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                //}
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.HR_GetHonorHeader.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.NoBukti,
                            Valisasi = m.Validasi ?? false,
                            Posted = m.Posting ?? false,
                            TglEnd = m.TglEnd?.ToString("yyyy-MM-dd"),
                            TipeHonor = m.TipeHonor,
                            Periode = m.TglStart?.ToString("yyyy-MM-dd"),
                            Keterangan = m.Keterangan,
                            FilterDokter = m.DokterID,
                            FilterDokterNama = m.NamaDOkter,
                        },
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== OTHER

        [HttpPost]
        public string Validasi(string id)
        {
            using (var s = new SIMEntities())
            {
                try
                {
                    var userid = User.Identity.GetUserId();
                    s.HR_HitungHonor_Validasi(id);
                    s.SaveChanges();

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"HitungHonor-Validasi; id:{id};".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);

                    return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                }
                catch (SqlException ex) { return HConvert.Error(ex); }
                catch (Exception ex) { return HConvert.Error(ex); }
            }
        }

        [HttpPost]
        public string BatalValidasi(string id)
        {
            using (var s = new SIMEntities())
            {
                try
                {
                    var userid = User.Identity.GetUserId();
                    s.HR_HitungHonor_Validasi_Cancel(id);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"HitungHonor-BatalValidasi; id:{id};".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);

                    return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                }
                catch (SqlException ex) { return HConvert.Error(ex); }
                catch (Exception ex) { return HConvert.Error(ex); }
            }
        }

        [HttpPost]
        public string Posting(string id)
        {
            using (var s = new SIMEntities())
            {
                try
                {
                    var userid = User.Identity.GetUserId();
                    s.HR_HitungHonor_Posting(id);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"HitungHonor-Posting; id:{id};".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);

                    return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                }
                catch (SqlException ex) { return HConvert.Error(ex); }
                catch (Exception ex) { return HConvert.Error(ex); }
            }
        }

        [HttpPost]
        public string BatalPosting(string id)
        {
            using (var s = new SIMEntities())
            {
                try
                {
                    var userid = User.Identity.GetUserId();
                    s.HR_HitungHonor_Posting_Cancel(id);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"HitungHonor-BatalPosting; id:{id};".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);

                    return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                }
                catch (SqlException ex) { return HConvert.Error(ex); }
                catch (Exception ex) { return HConvert.Error(ex); }
            }
        }

        #endregion

        #region ===== V I E W - P A S I E N

        [HttpPost]
        public string List_Pasien(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var nobukti = filter.FirstOrDefault(x => x.Key == "NoBukti")?.Value;
                    var proses = s.HR_GetHonorPasien.Where(x => x.NoBukti == nobukti).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.NoInvoice.Contains(x.Value) ||
                                y.NoReg.Contains(x.Value)
                            );
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBukti = x.NoBukti,
                        NoReg = x.NoReg,
                        Tanggal = x.Tanggal.ToString("dd/MM/yyyy"),
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                        NoInvoice = x.NoInvoice,
                        TotalHonor = x.TotalHonor,
                        Alat = x.TotalAlat,
                        Validasi = x.Validasi ?? false
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Detail_Pasien(string id, string noinvoice)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.HR_GetHonorPasien.FirstOrDefault(x => x.NoBukti == id && x.NoInvoice == noinvoice);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.HR_GetHonorPasien_byDokterIDNoBukti.Where(x => x.NoInvoice == noinvoice && x.NoBukti == id).ToList();

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoInvoice = m.NoInvoice,
                            NoBukti = m.NoBukti,
                            NoReg = m.NoReg,
                            NRM = m.NRM,
                            Nama = m.NamaPasien,
                            Honor = m.TotalHonor,
                            Alat = m.TotalAlat,
                            Validasi = m.Validasi ?? false
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            IdDokter = x.DokterID,
                            NamaDokter = x.NamaDOkter,
                            TglTindakan = x.TglTindakan.ToString("dd/MM/yyyy"),
                            Jasa = x.JasaName,
                            Kelas = x.Kelas,
                            Keterangan = x.Keterangan,
                            Qty = x.Qty,
                            Tarif = x.Tarif,
                            Diskon = x.Diskon,
                            Potongan = x.Potongan,
                            KetPotongan = x.KeteranganPotongan,
                            NoInvoice = x.NoInvoice,
                            Validasi = x.Validasi ?? false,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Save_Pasien(string _process, HitungHonorDetailPasienModel model)
        {
            using (var s = new SIMEntities())
            {
                try
                {
                    var userid = User.Identity.GetUserId();
                    if (_process == "DELETE")
                    {
                        s.HR_HitungHonor_DeletePasien(model.NoBukti, model.NoInvoice);
                        s.SaveChanges();
                    }

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"HitungHonorPasien-{_process}; id:{model.NoInvoice};".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                catch (SqlException ex) { return HConvert.Error(ex); }
                catch (Exception ex) { return HConvert.Error(ex); }
            }
            return HConvert.Success();
        }


        #endregion

        #region ===== V I E W - D O K T E R

        [HttpPost]
        public string List_Dokter(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var nobukti = filter.FirstOrDefault(x => x.Key == "NoBukti")?.Value;
                    var proses = s.HR_GetHonorDokter.Where(x => x.NoBukti == nobukti).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.DokterID.Contains(x.Value) ||
                                y.NamaDokter.Contains(x.Value)
                            );
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBukti = x.NoBukti,
                        Id = x.DokterID,
                        Nama = x.NamaDokter,
                        Honor = x.HonorNet,
                        Pajak = x.TotalPajak,
                        Rumkit = x.TotalRumkit,
                        RSU = x.TotalRSU,
                        Nett = x.HonorNet,
                        Alat = x.TotalAlat,
                        AkumulasiHonorSBLM = x.AkumulasiHonorSBLM,
                        OK = x.SudahOK ?? false,
                        Terbayar = x.Terbayar ?? false,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Detail_Dokter(string id, string dokterid)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.HR_GetHonorDokter.FirstOrDefault(x => x.NoBukti == id && x.DokterID == dokterid);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.HR_GetHonorPasien_byNoInvoiceNoBukti.Where(x => x.NoBukti == id && x.DokterID == dokterid).ToList();

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NamaDokter = m.NamaDokter,
                            TotalHonor = m.TotalHonor,
                            TotalPajak = m.TotalPajak,
                            AlatDokter = m.TotalAlat,
                            TotalRumkit = m.TotalRumkit,
                            TotalRSU = m.TotalRSU,
                            HonorNett = m.HonorNet,
                            AkumulasiHonorSebelumnya = m.AkumulasiHonorSBLM,
                            PajakDPP = m.DPP_4,
                            PajakTarif = m.Tarif_4,
                            PajakNominal = m.Pajak_4
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Pasien = x.Pasien,
                            NoReg = x.NoReg,
                            JnPasien = x.JenisPasien,
                            TglTindakan = x.TglTindakan.ToString("dd/MM/yyyy"),
                            JasaName = x.JasaName,
                            Kelas = x.Kelas,
                            Keterangan = x.Keterangan,
                            Qty = x.Qty,
                            Tarif = x.Tarif,
                            Diskon = x.Diskon,
                            Potongan = x.Potongan,
                            KetPotongan = x.KeteranganPotongan,
                            NoInvoice = x.NoInvoice,
                            Validasi = x.Validasi ?? false
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}