﻿using Honor.Entities.SIM;
using Honor.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Honor.Models.Honor;
using System.Net.Configuration;

namespace Honor.Controllers.Honor
{
    [Authorize]
    public class InputHonorLainController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.HR_HonorLainya.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => 
                            y.NoBukti.Contains(x.Value) ||
                            y.Dokter_Nama_Supplier.Contains(x.Value) ||
                            y.KeteranganHonor.Contains(x.Value) ||
                            y.Dokter_Kode_Supplier.Contains(x.Value) 
                            );
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBukti = x.NoBukti,
                        Tanggal = x.Tanggal.ToString("yyyy-MM-dd"),
                        Dokter_Kode_Supplier = x.Dokter_Kode_Supplier ?? "-",
                        Dokter_Nama_Supplier = x.Dokter_Nama_Supplier ?? "-",
                        Nilai = x.Nilai,
                        KeteranganHonor = x.KeteranganHonor,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, InputHonorLainModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = 0;
                        if (_process == "CREATE")
                        {
                            s.HR_HonorLain_Insert(model.Tanggal, model.Supplier_ID, model.KeteranganHonor, model.Nilai);
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.HR_HonorLain_Update(model.Tanggal, model.Supplier_ID, model.KeteranganHonor, model.Nilai, model.NoBukti);
                            s.SaveChanges();
                        }
                      

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"InputHonorLain-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.HR_HonorLainya.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.NoBukti,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                            Supplier_ID = m.Supplier_ID,
                            Dokter_Kode_Supplier = m.Dokter_Kode_Supplier,
                            Dokter_Nama_Supplier = m.Dokter_Nama_Supplier,
                            Nilai = m.Nilai,
                            KeteranganHonor = m.KeteranganHonor,
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - DOKTER

        [HttpPost]
        public string ListLookupDokter(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.HR_GetDokter.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.DokterID.Contains(x.Value) ||
                                y.NamaDOkter.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Supplier_ID = x.Supplier_ID,
                        Dokter_Kode_Supplier = x.DokterID,
                        Dokter_Nama_Supplier = x.NamaDOkter,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}