﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Honor.Models.Honor
{
    public class InputHonorLainModel
    {
        public DateTime Tanggal { get; set; }
        public short Supplier_ID { get; set; }
        public string KeteranganHonor { get; set; }
        public decimal Nilai { get; set; }
        public string NoBukti { get; set; }
    }
}
